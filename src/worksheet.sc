List(1,2,3,4).map( x => x*2)

"Test".toLowerCase.toList
  .groupBy( c => c ).map( (elem) => (elem._1,elem._2.length)).toList

"Test".toLowerCase.toList
  .foldLeft( Map[Char,Int]() ) { (acc,emitted) => emitted match {
    case emmited if acc.contains(emmited) => acc + (emitted -> (acc(emmited) + 1))
    case _ => acc + (emitted -> 1 )
  }}.toList

List("a","b").mkString

type Word = String
type Sentence = List[Word]
val c: Sentence = List("me","you")
c.mkString